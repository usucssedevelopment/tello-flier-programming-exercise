# tello-flier-programming-exercise #

This repository holds a simple programming exercise that involves with a program, called a Flier, that will automatically fly the Tello drone on one of several missions.  It includes the following:

* A description of the programming exercise (see "Programming Exercise Description.docx")
* A simulator program that acts like a Tello Drone and can be used for developing and testing a Tello Flier (see "simulator")
* Tello documentation (See "Tello SDK 2.0 User Guide.pdf)

Note, to run the simulator, open a terminal window, go to the simulatlor directory, type `java -jar tello-simulator-1.2.jar`.

## Contribution guidelines ##

* Make sure that there are executable test cases that provide good coverage
* Do a code review with Stephen Clyde or some other administrator assigned to the project.

## Who do I talk to? ##

* Stephen Clyde, Utah State University, Stephen.Clyde@usu.edu, 435-764-1596
